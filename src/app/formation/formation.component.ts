import { Component, OnInit } from '@angular/core';
import formations from '../../assets/formation.json';

@Component({
  selector: 'app-formation',
  templateUrl: './formation.component.html',
  styleUrls: ['./formation.component.scss']
})
export class FormationComponent implements OnInit {

  public formationsList:{date:string, diplome:string, lieu:string}[] = formations;

  constructor() { }

  ngOnInit(): void {
  }

}
