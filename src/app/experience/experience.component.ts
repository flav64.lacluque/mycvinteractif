import { Component, OnInit } from '@angular/core';
import experiences from '../../assets/experience.json'

@Component({
  selector: 'app-experience',
  templateUrl: './experience.component.html',
  styleUrls: ['./experience.component.scss']
})
export class ExperienceComponent implements OnInit {

  imagePath!: string;
  titre!: string;
  date!: string;
  public experiencesList:{name:string, date:string, tache1:string,tache2:string,tache3:string, imagePath:string}[] = experiences;

  constructor(){

  }

  ngOnInit(): void {
  }

}
